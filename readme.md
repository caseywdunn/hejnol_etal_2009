# Hejnol et al. 2009

This repository contains files for the analyses presented in:

> Hejnol, A, M Obst, A Stamatakis, M Ott, G Rouse, G Edgecombe, P Martinez, J Baguñà, X Bailly, U Jondelius, M Wiens, WEG Müller, Elaine Seaver, WC Wheeler, MQ Martindale, G Giribet, and CW Dunn (2009) Assessing the root of bilaterian animals with scalable phylogenomic methods. Proc. R. Soc. B. 276:4261-4270 [doi:10.1098/rspb.2009.0896](http://dx.doi.org/10.1098/rspb.2009.0896)


## Data

Files labeled with `.tre` extension are in nexus format. The newick trees can be excised from these files.


The files are:

- `Hejnol_etal_2009.nex` The nexus file with gene sequence alignments and comments indicating which genes were included in the submatrices.

- `04Jun08_25_33_boot_on_19ml_fixednodes.tre` The source for Figure 2. `fixednodes` refers to rounding of support values stored as node labels.

- `04Jun08_25_33_boot_on_19ml_stab88.tre` The source for Figure 3.

- `04Jun08_25_33_boot_on_19ml_stab90.tre` The source for Figure 4.

- `04Jun08_10May07reconstruction_boot_on_best.tre` The source for Supplementary Figure 1.

- `04Jun08pp_50_boot_on_best.tre` The source for Supplementary Figure 2.

- `04Jun08_25t_25p_boot_on_19ml.stab90.tre` The source for Supplementary Figure 4.

